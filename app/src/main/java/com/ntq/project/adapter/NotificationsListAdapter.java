package com.ntq.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ntq.project.R;

public class NotificationsListAdapter extends BaseAdapter {

    private Context mContext;
    private String[] mData;

    public NotificationsListAdapter(Context context, String[] data) {
        mContext = context;
        mData = data;
    }
    @Override
    public int getCount() {
        return mData.length;
    }

    @Override
    public Object getItem(int position) {
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_notification, null);
        }
        return convertView;
    }
}
