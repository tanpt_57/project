package com.ntq.project.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentSettingNotifications extends Fragment implements View.OnClickListener{

    private SharedPreferences prefs;

    private CheckedTextView chbUpdateToMyBuzz;
    private CheckedTextView chbAndGAlert;
    private CheckedTextView chbCheckMeOut;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_notifications, container, false);

        AndG parent = (AndG) getActivity();
        parent.setTitle(R.string.notification_title);
        parent.setButtonActionLeft(AndG.NAV_BACK, "");
        parent.setButtonActionRight(AndG.NAV_TEXT, getString(R.string.common_save));

        prefs = getActivity().getSharedPreferences(FragmentSetting.PREFS_FILE_NAME, Context.MODE_PRIVATE);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                save();
            }
        };

        getViews(view);
        loadDataFromPref();

        return view;
    }

    private void loadDataFromPref() {
        chbUpdateToMyBuzz.setChecked(prefs.getBoolean(FragmentSetting.PREF_NOTIFICATION_UPDATE_BUZZ, true));
        chbAndGAlert.setChecked(prefs.getBoolean(FragmentSetting.PREF_NOTIFICATION_ANDG_ALERT, true));
        chbCheckMeOut.setChecked(prefs.getBoolean(FragmentSetting.PREF_NOTIFICATION_CHECK_ME_OUT, true));
    }

    private void save() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FragmentSetting.PREF_NOTIFICATION_UPDATE_BUZZ, chbUpdateToMyBuzz.isChecked());
        editor.putBoolean(FragmentSetting.PREF_NOTIFICATION_ANDG_ALERT, chbAndGAlert.isChecked());
        editor.putBoolean(FragmentSetting.PREF_NOTIFICATION_CHECK_ME_OUT, chbCheckMeOut.isChecked());
        editor.commit();
        Toast.makeText(getActivity(), "Saved success", Toast.LENGTH_LONG).show();
        getFragmentManager().popBackStack();
    }

    private void getViews(View view) {
        chbUpdateToMyBuzz = (CheckedTextView) view.findViewById(R.id.chb_updates_to_my_buzz);
        chbAndGAlert = (CheckedTextView) view.findViewById(R.id.chb_andg_alert);
        chbCheckMeOut = (CheckedTextView) view.findViewById(R.id.chb_check_me_out);

        chbUpdateToMyBuzz.setOnClickListener(this);
        chbAndGAlert.setOnClickListener(this);
        chbCheckMeOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        CheckedTextView ctv = (CheckedTextView) view;
        ctv.setChecked(!ctv.isChecked());
    }
}
