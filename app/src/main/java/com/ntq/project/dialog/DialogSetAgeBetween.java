package com.ntq.project.dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.ntq.project.R;
import com.ntq.project.fragment.FragmentSearchFriendSetting;

public class DialogSetAgeBetween extends DialogFragment implements View.OnClickListener{

    private NumberPicker minAge;
    private NumberPicker maxAge;

    @Nullable
    @Override
    @TargetApi(11)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_age_between, container, false);

        getDialog().setTitle("of age between");

        minAge = (NumberPicker) v.findViewById(R.id.minAge);
        maxAge = (NumberPicker) v.findViewById(R.id.maxAge);

        minAge.setMinValue(18);
        minAge.setMaxValue(120);
        minAge.setValue(getArguments().getInt("ageMin"));

        maxAge.setMinValue(18);
        maxAge.setMaxValue(120);
        maxAge.setValue(getArguments().getInt("ageMax"));

        Button btnOk = (Button) v.findViewById(R.id.btnOk);
        Button btnCancel = (Button) v.findViewById(R.id.btnCancel);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return v;
    }

    @Override
    @TargetApi(11)
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnOk:
                int minValue = minAge.getValue();
                int maxValue = maxAge.getValue();

                if (minValue > maxValue) {
                    Toast.makeText(getActivity(), minValue+" is not bigger than "+maxValue, Toast.LENGTH_LONG).show();
                }
                else {
                    Intent data = new Intent();
                    data.putExtra("ageMin", minValue);
                    data.putExtra("ageMax", maxValue);
                    getTargetFragment().onActivityResult(FragmentSearchFriendSetting.TARGET_AGE_BETWEEN, Activity.RESULT_OK, data);
                    dismiss();
                }
                break;
            case R.id.btnCancel:
                dismiss();
        }
    }
}
