package com.ntq.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntq.project.R;
import com.ntq.project.model.Point;

import java.util.List;

public class BuyPointsListAdapter extends BaseAdapter {

    private List<Point> mPointList;
    private Context mContext;

    public BuyPointsListAdapter(Context context, List<Point> pointList) {
        mContext = context;
        mPointList = pointList;
    }

    @Override
    public int getCount() {
        return mPointList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPointList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_buy_point, null);

            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.image);
            holder.title = (TextView) convertView.findViewById(R.id.text_title);
            holder.description = (TextView) convertView.findViewById(R.id.text_desc);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Point point = mPointList.get(position);
        holder.imageView.setImageResource(point.getImageResoureId());
        holder.title.setText(point.getTitle());
        holder.description.setText(point.getDescription());

        return convertView;
    }


    public class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView description;
    }

}
