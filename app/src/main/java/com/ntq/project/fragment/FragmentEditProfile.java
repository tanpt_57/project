package com.ntq.project.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;
import com.ntq.project.R;
import com.ntq.project.dialog.DialogEditProfile;

public class FragmentEditProfile extends Fragment implements View.OnClickListener {

    public static final String PREFS_FILE_NAME = "EditProfilePrefs";
    public static final String PREF_NAME = "name";
    public static final String PREF_GENDER = "gender";
    public static final String PREF_RELATIONSHIP = "relationship";

    private SharedPreferences prefs;

    private AndG parent;

    private String[] relationshipStatus;

    private TextView tvName, tvGender, tvRelationship;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);

        parent = (AndG) getActivity();

        parent.setTitle(R.string.edit_my_profile_title);
        parent.setButtonActionLeft(AndG.NAV_TEXT, getString(R.string.common_cancel));
        parent.setButtonActionRight(AndG.NAV_TEXT, getString(R.string.common_save));

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                onSave();
            }
        };

        relationshipStatus = getResources().getStringArray(R.array.relationshipStatusEdit);

        prefs = getActivity().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);

        getViews(view);

        loadDataFromPref();

        return view;
    }

    public void onSave() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_NAME, tvName.getText().toString());
        editor.putString(PREF_GENDER, tvGender.getText().toString());
        editor.putString(PREF_RELATIONSHIP, tvRelationship.getText().toString());
        editor.commit();

        Toast.makeText(getActivity(), "Save edit success", Toast.LENGTH_LONG).show();
    }

    private void loadDataFromPref() {
        tvName.setText(prefs.getString(PREF_NAME, "AndG"));
        tvGender.setText(prefs.getString(PREF_GENDER, getString(R.string.male)));
        tvRelationship.setText(prefs.getString(PREF_RELATIONSHIP, "Ask me"));
    }

    private void getViews(View view) {
        RelativeLayout itemName = (RelativeLayout) view.findViewById(R.id.item_name);
        RelativeLayout itemGender = (RelativeLayout) view.findViewById(R.id.item_gender);
        RelativeLayout itemRelationship = (RelativeLayout) view.findViewById(R.id.item_relationship);

        itemName.setOnClickListener(this);
        itemGender.setOnClickListener(this);
        itemRelationship.setOnClickListener(this);

        tvName = (TextView) view.findViewById(R.id.tv_name);
        tvGender = (TextView) view.findViewById(R.id.tv_gender);
        tvRelationship = (TextView) view.findViewById(R.id.tv_relationship_status);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.item_name:
                final EditText editName = new EditText(getActivity());
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Set Name")
                        .setView(editName)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tvName.setText(editName.getText().toString());
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create().show();
                break;
            case R.id.item_gender:
                final String[] genders = new String[]{getString(R.string.male), getString(R.string.female)};
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Set Gender")
                        .setItems(genders, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tvGender.setText(genders[which]);
                            }
                        })
                        .create().show();
                break;
            case R.id.item_relationship:
                DialogEditProfile dialog = new DialogEditProfile();
                Bundle args = new Bundle();
                args.putStringArray("editProfile", relationshipStatus);
                args.putString("itemSelected", tvRelationship.getText().toString());
                dialog.setArguments(args);
                dialog.setTargetFragment(this, 0);
                dialog.show(parent.getSupportFragmentManager(), dialog.getClass().getName());
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                int position = data.getIntExtra("relationship", 0);
                tvRelationship.setText(relationshipStatus[position]);
            }
        }
    }
}
