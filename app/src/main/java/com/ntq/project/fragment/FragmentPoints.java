package com.ntq.project.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.adapter.BuyPointsListAdapter;
import com.ntq.project.interfaces.OnClickActionBarListener;
import com.ntq.project.model.Point;

import java.util.ArrayList;
import java.util.List;

public class FragmentPoints extends Fragment {

    private AndG parent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_point, container, false);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.points);
        parent.setButtonActionLeft(AndG.NAV_MENU, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(4);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                parent.getSlidingMenu().showMenu();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        TextView tvTotalPoints = (TextView) view.findViewById(R.id.tv_total_points);
        int totalPoints = getActivity().getSharedPreferences(AndG.PREFS_FILE_NAME, Context.MODE_PRIVATE)
                .getInt(AndG.PREF_TOTAL_POINTS, 0);
        tvTotalPoints.setText(totalPoints+"");

        ListView listBuyPoints = (ListView) view.findViewById(R.id.list_buy_points);
        List<Point> pointList = new ArrayList<>();
        pointList.add(new Point(R.drawable.ic_lookatme, getString(R.string.point_title_look), getString(R.string.point_des_look)));
        pointList.add(new Point(R.drawable.ic_whocheckmeout, getString(R.string.point_title_who), getString(R.string.point_des_who)));
        pointList.add(new Point(R.drawable.ic_winkboom, getString(R.string.point_title_wink), getString(R.string.point_des_wink)));
        pointList.add(new Point(R.drawable.ic_whofavme, getString(R.string.point_title_favourite), getString(R.string.point_des_favourite)));
        pointList.add(new Point(R.drawable.ic_gift_point, getString(R.string.point_title_gift), getString(R.string.point_des_gift)));

        BuyPointsListAdapter adapter = new BuyPointsListAdapter(getActivity(), pointList);
        listBuyPoints.setAdapter(adapter);

        return view;
    }

}
