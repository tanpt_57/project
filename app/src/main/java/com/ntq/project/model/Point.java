package com.ntq.project.model;

public class Point {

    private int imageResoureId;
    private String title;
    private String description;

    public Point(int imageId, String title, String desc) {
        this.imageResoureId = imageId;
        this.title = title;
        this.description = desc;
    }

    public int getImageResoureId() {
        return imageResoureId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
