package com.ntq.project.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;

import com.ntq.project.R;
import com.ntq.project.adapter.EditProfileListAdapter;

public class DialogEditProfile extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_relationship_status, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        String[] relationshipStatus = getArguments().getStringArray("editProfile");
        String itemSelected = getArguments().getString("itemSelected");

        ListView lvRelationshipStatus = (ListView) v.findViewById(R.id.lv_relationship_status);
        final EditProfileListAdapter adapter = new EditProfileListAdapter(getActivity(), relationshipStatus, this, itemSelected);
        lvRelationshipStatus.setAdapter(adapter);

        return v;
    }

}
