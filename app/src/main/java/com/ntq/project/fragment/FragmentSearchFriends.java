package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentSearchFriends extends Fragment implements View.OnClickListener {

    private AndG parent;

    private TextView textWhoCheckMe;
    private TextView textChangeView;
    private ImageButton btnTopSwitcher;

    private FrameLayout layoutContainer;

    private boolean showListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search_friend, container, false);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.search_friends_title);
        parent.setButtonActionLeft(AndG.NAV_MENU, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(4);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                parent.getSlidingMenu().showMenu();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        TextView textSearchFriend = (TextView) view.findViewById(R.id.text_search_friends);
        textWhoCheckMe = (TextView) view.findViewById(R.id.text_whocheckme);
        textChangeView = (TextView) view.findViewById(R.id.text_change_view);
        btnTopSwitcher = (ImageButton) view.findViewById(R.id.btn_top_switcher);

        textSearchFriend.setOnClickListener(this);
        textWhoCheckMe.setOnClickListener(this);
        textChangeView.setOnClickListener(this);
        btnTopSwitcher.setOnClickListener(this);

        layoutContainer = (FrameLayout) view.findViewById(R.id.container_search_friends);

        changeView(showListView);

        return view;
    }

    private boolean isWhoCheckMeLock = true;
    private boolean isTopSwitcherCollaps = false;

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.text_search_friends:
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment fragment = new FragmentSearchFriendSetting();
                transaction.replace(R.id.content_body, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.text_whocheckme:
                if (isWhoCheckMeLock) {
                    textWhoCheckMe.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pannel_whocheckme, 0, 0);
                    isWhoCheckMeLock = false;
                }
                else {
                    textWhoCheckMe.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pannel_whocheckme_lock, 0, 0);
                    isWhoCheckMeLock = true;
                }
                break;
            case R.id.text_change_view:
                changeView(showListView);
                break;
            case R.id.btn_top_switcher:
                if (isTopSwitcherCollaps) {
                    btnTopSwitcher.setBackgroundResource(R.drawable.top_switcher);
                    layoutContainer.setVisibility(View.VISIBLE);
                }
                else {
                    btnTopSwitcher.setBackgroundResource(R.drawable.top_switcher_collaps);
                    layoutContainer.setVisibility(View.INVISIBLE);
                }
                isTopSwitcherCollaps = !isTopSwitcherCollaps;
            default:
                break;
        }
    }

    public void changeView(boolean showListView) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        Fragment fragment;

        if (showListView) {
            textChangeView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pannel_grid, 0, 0);
            textChangeView.setText(R.string.grid_view);
            fragment = new FragmentSearchFriendList();
        }
        else {
            textChangeView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pannel_list, 0, 0);
            textChangeView.setText(R.string.list_view);
            fragment = new FragmentSearchFriendGrid();
        }
        this.showListView = !showListView;

        transaction.replace(R.id.container_search_friends, fragment).commit();
    }

}
