package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.adapter.GiveGiftListAdapter;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentGiveGift extends Fragment {

    private AndG parent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_give_gift, container, false);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.title_textview_my_profile_give_gift);
        parent.setButtonActionLeft(AndG.NAV_BACK, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(4);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        ListView lvGiveGift = (ListView) view.findViewById(R.id.lv_give_gift);

        String[] listGiveGift = getResources().getStringArray(R.array.give_gift_list);
        GiveGiftListAdapter adapter = new GiveGiftListAdapter(getActivity(), R.layout.item_list_give_gift, listGiveGift);
        lvGiveGift.setAdapter(adapter);

        lvGiveGift.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_body, new FragmentGift())
                        .addToBackStack(null)
                        .commit();
            }
        });
        return view;
    }

}
