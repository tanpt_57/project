package com.ntq.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.ntq.project.R;

import java.util.HashMap;

public class EthnicityListAdapter extends BaseAdapter {

    private Context mContext;
    private HashMap<String, Boolean> mData;
    private String[] listEthnicity;

    public EthnicityListAdapter(Context context, HashMap<String, Boolean> data) {
        mContext = context;
        mData = data;
        listEthnicity = mContext.getResources().getStringArray(R.array.ethnicity_list);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(listEthnicity[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_ethnicity, null);
        }

        final CheckedTextView itemEthnicity = (CheckedTextView) convertView.findViewById(R.id.item_ethnicity);
        itemEthnicity.setText(listEthnicity[position]);
        itemEthnicity.setChecked(mData.get(listEthnicity[position]));

        itemEthnicity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemEthnicity.setChecked(!itemEthnicity.isChecked());
                if (position == 0) {
                    for (String s : listEthnicity) {
                        mData.put(s, itemEthnicity.isChecked());
                    }
                    notifyDataSetChanged();
                }
                else {
                    if (mData.get(listEthnicity[0]))
                        mData.put(listEthnicity[0], false);

                    mData.put(listEthnicity[position], itemEthnicity.isChecked());

                    int countCheck = 0;
                    for (String s : listEthnicity) {
                        if (mData.get(s))
                            countCheck++;
                    }
                    if (countCheck == listEthnicity.length-1)
                        mData.put(listEthnicity[0], true);

                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }
}
