package com.ntq.project.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentSettingDistance extends Fragment implements View.OnClickListener {

    private AndG parent;

    private CheckedTextView distanceMiles;
    private CheckedTextView distanceKilometers;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_distance, container, false);

        parent = (AndG) getActivity();

        parent.setTitle(R.string.distance_in);
        parent.setButtonActionLeft(AndG.NAV_BACK, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(5);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        getViews(view);

        return view;
    }

    private void getViews(View view) {
        distanceMiles = (CheckedTextView) view.findViewById(R.id.distance_miles);
        distanceKilometers = (CheckedTextView) view.findViewById(R.id.distance_kilometers);

        String typeDistance = getArguments().getString("typeDistance");

        if (typeDistance.equals(getString(R.string.settings_account_distance_miles))) {
            distanceMiles.setChecked(true);
        }
        else {
            distanceKilometers.setChecked(true);
        }

        distanceMiles.setOnClickListener(this);
        distanceKilometers.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String typeDistance = null;

        switch (v.getId()) {
            case R.id.distance_miles:
                distanceMiles.setChecked(true);
                distanceKilometers.setChecked(false);
                typeDistance = getString(R.string.settings_account_distance_miles);
                break;
            case R.id.distance_kilometers:
                distanceMiles.setChecked(false);
                distanceKilometers.setChecked(true);
                typeDistance = getString(R.string.settings_account_distance_kilometers);
                break;
        }

        getActivity().getSharedPreferences(FragmentSetting.PREFS_FILE_NAME, Context.MODE_PRIVATE)
                .edit().putString(FragmentSetting.PREF_DISTANCE_IN, typeDistance)
                .commit();

        getFragmentManager().popBackStack();
    }
}
