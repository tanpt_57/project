package com.ntq.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ntq.project.R;

public class GiveGiftListAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private String[] mDatas;
    private int layout_id;

    public GiveGiftListAdapter(Context context, int resource, String[] datas) {
        super(context, resource, datas);
        mContext = context;
        mDatas = datas;
        layout_id = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layout_id, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.text_view);
        textView.setText(mDatas[position]);

        return convertView;
    }
}
