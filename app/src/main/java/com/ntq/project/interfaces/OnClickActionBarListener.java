package com.ntq.project.interfaces;

public interface OnClickActionBarListener {

    public void onClickActionBarLeft();
    public void onClickActionBarRight();

}
