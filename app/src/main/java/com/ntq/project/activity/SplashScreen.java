package com.ntq.project.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.ntq.project.R;
import com.ntq.project.adapter.ImagePagerAdapter;

public class SplashScreen extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        int[] splashList = new int[] {
                R.drawable.splash_intro1,
                R.drawable.splash_intro2,
                R.drawable.splash_intro3,
                R.drawable.splash_intro4
        };

        ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter(this, splashList);
        mPager.setAdapter(adapter);

        Button btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        Button btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
