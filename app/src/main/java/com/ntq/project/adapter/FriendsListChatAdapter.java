package com.ntq.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;

import java.util.List;

public class FriendsListChatAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mData;

    public FriendsListChatAdapter(Context context, List<String> data) {
        mContext = context;
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_friends_chat, null);
            holder = new ViewHolder();
            holder.btnDelete = (Button) convertView.findViewById(R.id.btn_delete);
            holder.textChatByMe = (TextView) convertView.findViewById(R.id.text_chat_by_me);
            holder.textChatToMe = (TextView) convertView.findViewById(R.id.text_chat_to_me);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (AndG.isDeleteHistory)
            holder.btnDelete.setVisibility(View.VISIBLE);
        else
            holder.btnDelete.setVisibility(View.GONE);

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, position + "", Toast.LENGTH_LONG).show();
                mData.remove(position);
                notifyDataSetChanged();
            }
        });

        if (mData.get(position).equals("1")) {
            holder.textChatByMe.setVisibility(View.GONE);
            holder.textChatToMe.setVisibility(View.VISIBLE);
        }
        else {
            holder.textChatToMe.setVisibility(View.GONE);
            holder.textChatByMe.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    static class ViewHolder {
        public TextView textChatToMe;
        public TextView textChatByMe;
        public Button btnDelete;
    }
}
