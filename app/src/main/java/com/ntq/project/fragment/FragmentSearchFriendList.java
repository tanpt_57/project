package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ntq.project.adapter.SearchFriendListAdpater;

public class FragmentSearchFriendList extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView lvFriends = new ListView(getActivity());
        String[] data = new String[]{"1", "2", "3", "4", "5","1", "2", "3", "4", "5"};
        SearchFriendListAdpater adapter = new SearchFriendListAdpater(getActivity(), data);
        lvFriends.setAdapter(adapter);
        return lvFriends;
    }

}
