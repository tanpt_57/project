package com.ntq.project.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;

public class FragmentGift extends Fragment {

    private AndG parent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_view, container, false);

        parent = (AndG) getActivity();
        parent.setTitle("All");

        GridView gridView = (GridView) view.findViewById(R.id.grid_view);
        String[] data = new String[] {"1", "2", "3","1", "2", "3","1", "2", "3","1", "2", "3","1", "2", "3"};
        MyAdapter adapter = new MyAdapter(getActivity(), data);
        gridView.setAdapter(adapter);
        return view;
    }

    class MyAdapter extends BaseAdapter {

        private Context mContext;
        private String[] mData;

        public MyAdapter(Context context, String[] data) {
            mContext = context;
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.length;
        }

        @Override
        public Object getItem(int position) {
            return mData[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_list_gift, null);
            }
            return convertView;
        }
    }
}
