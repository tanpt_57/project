package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.ntq.project.R;
import com.ntq.project.adapter.SearchFriendGridAdapter;

public class FragmentSearchFriendGrid extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_view, container, false);
        GridView gvFriends = (GridView) view.findViewById(R.id.grid_view);
        String[] data = new String[]{"1", "2", "3", "4", "5","1", "2", "3", "4", "5","1", "2", "3", "4", "5"};
        SearchFriendGridAdapter adapter = new SearchFriendGridAdapter(getActivity(), data);
        gvFriends.setAdapter(adapter);
        return view;
    }

}
