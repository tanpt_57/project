package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentDeactivateAccount extends Fragment {

    private AndG parent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deactivate_account, container, false);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.settings_account_deactivate);
        parent.setButtonActionLeft(AndG.NAV_BACK, "");
        parent.setButtonActionRight(AndG.NAV_NONE, "");

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {

            }
        };

        return view;
    }

}
