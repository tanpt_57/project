package com.ntq.project.fragment;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentAuction extends Fragment {

    private AndG parent;

    private LinearLayout layoutContainer;
    private ImageButton btnTopSwitcher;
    private boolean isTopSwitcherCollaps = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.auction_activity, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.look_at_me);
        parent.setButtonActionLeft(AndG.NAV_MENU, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(4);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                parent.getSlidingMenu().showMenu();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        layoutContainer = (LinearLayout) view.findViewById(R.id.containter_feature);
        btnTopSwitcher = (ImageButton) view.findViewById(R.id.btn_top_switcher);
        btnTopSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTopSwitcherCollaps) {
                    btnTopSwitcher.setBackgroundResource(R.drawable.top_switcher);
                    layoutContainer.setVisibility(View.VISIBLE);
                }
                else {
                    btnTopSwitcher.setBackgroundResource(R.drawable.top_switcher_collaps);
                    layoutContainer.setVisibility(View.INVISIBLE);
                }
                isTopSwitcherCollaps = !isTopSwitcherCollaps;
            }
        });

        addRow(2);
        addRow(4);
        addRow(5);
        addRow(5);
        addRow(5);
        addRow(5);

        return view;
    }

    private void addRow(int numItems) {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.HORIZONTAL);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, width/numItems);
        layout.setLayoutParams(params);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < numItems; i++) {
            FrameLayout itemFeatureMe = (FrameLayout) inflater.inflate(R.layout.item_feature_me, layoutContainer, false);
            TextView tvTime = (TextView) itemFeatureMe.findViewById(R.id.tv_time);
            if (numItems == 2)
                tvTime.setText("12h remainter");
            else
                tvTime.setText("12h");
            layout.addView(itemFeatureMe);
        }

        layoutContainer.addView(layout);
    }
}
