package com.ntq.project.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.activity.SplashScreen;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentSetting extends Fragment implements View.OnClickListener {

    public static final String PREFS_FILE_NAME = "SettingPrefs";
    public static final String PREF_SOUND_ON = "soundOn";
    public static final String PREF_VIBRATION_ON = "vibrationOn";
    public static final String PREF_NOTIFICATIONS = "notifications";
    public static final String PREF_DISTANCE_IN = "distanceIn";
    public static final String PREF_DEACTIVATE_ACCOUNT = "deactivateAccount";
    public static final String PREF_NOTIFICATION_UPDATE_BUZZ = "updateToMyBuzz";
    public static final String PREF_NOTIFICATION_ANDG_ALERT = "andGAlert";
    public static final String PREF_NOTIFICATION_CHAT_MSG = "chatMessages";
    public static final String PREF_NOTIFICATION_CHECK_ME_OUT = "someoneCheckMeOut";

    private AndG parent;

    private SharedPreferences prefs;

    private CheckedTextView chbSoundOn, chbVibrationOn;
    private TextView textDistanceType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_setting, container, false);

        parent = (AndG) getActivity();

        parent.setTitle(R.string.settings);
        parent.setButtonActionLeft(AndG.NAV_MENU, "");
        parent.setButtonActionRight(AndG.NAV_TEXT, getString(R.string.logout));

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                parent.getSlidingMenu().showMenu();
            }

            @Override
            public void onClickActionBarRight() {
                logout();
            }
        };

        getViews(view);
        getDataFromPref();

        return view;
    }

    private void getDataFromPref() {
        prefs = getActivity().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        chbSoundOn.setChecked(prefs.getBoolean(PREF_SOUND_ON, true));
        chbVibrationOn.setChecked(prefs.getBoolean(PREF_VIBRATION_ON, true));
        textDistanceType.setText(prefs.getString(PREF_DISTANCE_IN, getString(R.string.settings_account_distance_miles)));
    }

    private void getViews(View view) {
        TextView textNotification = (TextView) view.findViewById(R.id.text_notifications);
        TextView textChangePassword = (TextView) view.findViewById(R.id.text_change_password);
        TextView textDeactivateAccount = (TextView) view.findViewById(R.id.text_deactivate_account);
        TextView textTermsOfService = (TextView) view.findViewById(R.id.text_terms_of_service);
        TextView textDistance = (TextView) view.findViewById(R.id.text_distance);
        textDistanceType = (TextView) view.findViewById(R.id.text_distance_type);
        chbSoundOn = (CheckedTextView) view.findViewById(R.id.chb_sound_on);
        chbVibrationOn = (CheckedTextView) view.findViewById(R.id.chb_vibration_on);

        textNotification.setOnClickListener(this);
        textChangePassword.setOnClickListener(this);
        textDeactivateAccount.setOnClickListener(this);
        textTermsOfService.setOnClickListener(this);
        textDistance.setOnClickListener(this);
        chbSoundOn.setOnClickListener(this);
        chbVibrationOn.setOnClickListener(this);
    }

    @TargetApi(11)
    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.logout))
                .setMessage("Are you sure?")
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences pref = getActivity().getSharedPreferences(AndG.PREFS_FILE_NAME, Context.MODE_PRIVATE);
                        pref.edit().putBoolean(AndG.PREF_IS_LOGIN, false).commit();

                        Intent intent = new Intent(getActivity(), SplashScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.common_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create().show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.chb_sound_on:
                chbSoundOn.setChecked(!chbSoundOn.isChecked());
                prefs.edit().putBoolean(PREF_SOUND_ON, chbSoundOn.isChecked()).commit();
                break;
            case R.id.chb_vibration_on:
                chbVibrationOn.setChecked(!chbVibrationOn.isChecked());
                prefs.edit().putBoolean(PREF_VIBRATION_ON, chbVibrationOn.isChecked()).commit();
                break;
            case R.id.text_notifications:
                replaceFragment(new FragmentSettingNotifications());
                break;
            case R.id.text_change_password:
                replaceFragment(new FragmentChangePassword());
                break;
            case R.id.text_deactivate_account:
                replaceFragment(new FragmentDeactivateAccount());
                break;
            case R.id.text_terms_of_service:
                replaceFragment(new FragmentTermsOfService());
                break;
            case R.id.text_distance:
                FragmentSettingDistance fragment = new FragmentSettingDistance();
                Bundle args = new Bundle();
                args.putString("typeDistance", textDistanceType.getText().toString());
                fragment.setArguments(args);
                replaceFragment(fragment);
                break;
            default:
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = parent.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_body, fragment, getClass().getName())
                .addToBackStack(null)
                .commit();
    }

}
