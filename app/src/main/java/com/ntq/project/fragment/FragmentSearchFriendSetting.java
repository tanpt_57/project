package com.ntq.project.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.dialog.DialogSetAgeBetween;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentSearchFriendSetting extends Fragment implements View.OnClickListener {

    public static final String PREFS_FILE_NAME = "SearchFriendSetting";
    public static final String PREF_SHOW_ME = "showMe";
    public static final String PREF_INTERESTED_IN = "interestedIn";
    public static final String PREF_LOCATION = "location";
    public static final String PREF_AGE_MIN = "ageMin";
    public static final String PREF_AGE_MAX = "ageMax";

    private SharedPreferences prefs;

    private TextView textEthnicity;
    private TextView textAgeBetween;

    private ToggleButton toggleBtnShowMeWoman, toggleBtnShowMeMan, toggleBtnShowMeManAndWoman;
    private ToggleButton toggleBtnInterestedWoman, toggleBtnInterestedMan, toggleBtnInterestedManAndWoman;
    private ToggleButton toggleBtnNear, toggleBtnState, toggleBtnCity, toggleBtnCountry, toggleBtnWorld;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_friends_setting_fragemt, container, false);

        AndG parent = (AndG) getActivity();
        parent.setTitle(R.string.search_setting_title);
        parent.setButtonActionLeft(AndG.NAV_TEXT, "Cancel");
        parent.setButtonActionRight(AndG.NAV_TEXT, "Search");

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                searchFriend();
            }
        };

        prefs = getActivity().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);

        getViews(view);
        loadDataFromPref();

        return view;
    }

    private void searchFriend() {

    }

    private void loadDataFromPref() {
        switch (prefs.getInt(PREF_SHOW_ME, R.id.toggle_btn_show_me_man_and_woman)) {
            case R.id.toggle_btn_show_me_man:
                toggleBtnShowMeMan.setChecked(true);
                break;
            case R.id.toggle_btn_show_me_woman:
                toggleBtnShowMeWoman.setChecked(true);
                break;
            case R.id.toggle_btn_show_me_man_and_woman:
                toggleBtnShowMeManAndWoman.setChecked(true);
                break;
        }
        switch (prefs.getInt(PREF_INTERESTED_IN, R.id.toggle_btn_interested_man_and_woman)) {
            case R.id.toggle_btn_interested_man:
                toggleBtnInterestedMan.setChecked(true);
                break;
            case R.id.toggle_btn_interested_woman:
                toggleBtnInterestedWoman.setChecked(true);
                break;
            case R.id.toggle_btn_interested_man_and_woman:
                toggleBtnInterestedManAndWoman.setChecked(true);
                break;
        }
        switch (prefs.getInt(PREF_LOCATION, R.id.toggle_btn_near)) {
            case R.id.toggle_btn_near:
                toggleBtnNear.setChecked(true);
                break;
            case R.id.toggle_btn_city:
                toggleBtnCity.setChecked(true);
                break;
            case R.id.toggle_btn_state:
                toggleBtnState.setChecked(true);
                break;
            case R.id.toggle_btn_country:
                toggleBtnCountry.setChecked(true);
                break;
            case R.id.toggle_btn_world:
                toggleBtnWorld.setChecked(true);
                break;
        }

        textAgeBetween.setText(prefs.getInt(PREF_AGE_MIN, 18) + " and " + prefs.getInt(PREF_AGE_MAX, 120));
        textEthnicity.setText(getEthnicity());
    }

    private void getViews(View view) {
        textEthnicity = (TextView) view.findViewById(R.id.text_ethnicity);
        textAgeBetween = (TextView) view.findViewById(R.id.text_age_between);

        toggleBtnShowMeMan = (ToggleButton) view.findViewById(R.id.toggle_btn_show_me_man);
        toggleBtnShowMeWoman = (ToggleButton) view.findViewById(R.id.toggle_btn_show_me_woman);
        toggleBtnShowMeManAndWoman = (ToggleButton) view.findViewById(R.id.toggle_btn_show_me_man_and_woman);

        toggleBtnInterestedMan = (ToggleButton) view.findViewById(R.id.toggle_btn_interested_man);
        toggleBtnInterestedWoman = (ToggleButton) view.findViewById(R.id.toggle_btn_interested_woman);
        toggleBtnInterestedManAndWoman = (ToggleButton) view.findViewById(R.id.toggle_btn_interested_man_and_woman);

        toggleBtnNear = (ToggleButton) view.findViewById(R.id.toggle_btn_near);
        toggleBtnState = (ToggleButton) view.findViewById(R.id.toggle_btn_state);
        toggleBtnCity = (ToggleButton) view.findViewById(R.id.toggle_btn_city);
        toggleBtnCountry = (ToggleButton) view.findViewById(R.id.toggle_btn_country);
        toggleBtnWorld = (ToggleButton) view.findViewById(R.id.toggle_btn_world);

        textEthnicity.setOnClickListener(this);
        textAgeBetween.setOnClickListener(this);

        toggleBtnShowMeMan.setOnClickListener(onClickShowMe);
        toggleBtnShowMeWoman.setOnClickListener(onClickShowMe);
        toggleBtnShowMeManAndWoman.setOnClickListener(onClickShowMe);

        toggleBtnInterestedMan.setOnClickListener(onClickInterested);
        toggleBtnInterestedWoman.setOnClickListener(onClickInterested);
        toggleBtnInterestedManAndWoman.setOnClickListener(onClickInterested);

        toggleBtnNear.setOnClickListener(onClickLocation);
        toggleBtnState.setOnClickListener(onClickLocation);
        toggleBtnCity.setOnClickListener(onClickLocation);
        toggleBtnCountry.setOnClickListener(onClickLocation);
        toggleBtnWorld.setOnClickListener(onClickLocation);
    }

    private View.OnClickListener onClickShowMe = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            toggleBtnShowMeMan.setChecked(false);
            toggleBtnShowMeWoman.setChecked(false);
            toggleBtnShowMeManAndWoman.setChecked(false);
            ((ToggleButton) v).setChecked(true);
            prefs.edit().putInt(PREF_SHOW_ME, v.getId()).commit();
        }
    };

    private View.OnClickListener onClickInterested = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            toggleBtnInterestedMan.setChecked(false);
            toggleBtnInterestedWoman.setChecked(false);
            toggleBtnInterestedManAndWoman.setChecked(false);
            ((ToggleButton) v).setChecked(true);
            prefs.edit().putInt(PREF_INTERESTED_IN, v.getId()).commit();
        }
    };

    private View.OnClickListener onClickLocation = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            toggleBtnNear.setChecked(false);
            toggleBtnCity.setChecked(false);
            toggleBtnCountry.setChecked(false);
            toggleBtnState.setChecked(false);
            toggleBtnWorld.setChecked(false);
            ((ToggleButton) v).setChecked(true);
            prefs.edit().putInt(PREF_LOCATION, v.getId()).commit();
        }
    };

    public static final int TARGET_AGE_BETWEEN = 2;

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.text_ethnicity:
                FragmentSearchFriendEditEthnicity fragment = new FragmentSearchFriendEditEthnicity();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_body, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.text_age_between:
                DialogSetAgeBetween dialog = new DialogSetAgeBetween();
                Bundle args = new Bundle();
                args.putInt("ageMin", prefs.getInt(PREF_AGE_MIN, 18));
                args.putInt("ageMax", prefs.getInt(PREF_AGE_MAX, 120));
                dialog.setArguments(args);
                dialog.setTargetFragment(this, TARGET_AGE_BETWEEN);
                dialog.show(getFragmentManager(), "DialogAgeBetween");
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TARGET_AGE_BETWEEN:
                if (resultCode == Activity.RESULT_OK) {
                    int ageMin = data.getIntExtra("ageMin", 18);
                    int ageMax = data.getIntExtra("ageMax", 120);

                    textAgeBetween.setText(ageMin + " and " + ageMax);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PREF_AGE_MIN, ageMin);
                    editor.putInt(PREF_AGE_MAX, ageMax);
                    editor.commit();
                }
                break;
            default:
                break;
        }
    }

    private String getEthnicity() {
        String[] listEthnicity = getResources().getStringArray(R.array.ethnicity_list);
        String txtEthnicity = "";
        int countCheck = 0;
        for (String s : listEthnicity) {
            if (prefs.getBoolean(s, false)) {
                txtEthnicity += s + "\n";
                countCheck++;
            }
        }

        if (countCheck == listEthnicity.length || countCheck == 0)
            txtEthnicity = listEthnicity[0];

        return txtEthnicity;
    }
}
