package com.ntq.project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.ntq.project.R;

public class EditProfileListAdapter extends BaseAdapter {

    private Context mContext;
    private String[] mData;
    private DialogFragment mDialog;
    private String itemSelected;

    public EditProfileListAdapter(Context context, String[] data, DialogFragment dialog, String itemSelected) {
        mContext = context;
        mData = data;
        mDialog = dialog;
        this.itemSelected = itemSelected;
    }

    @Override
    public int getCount() {
        return mData.length;
    }

    @Override
    public Object getItem(int position) {
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_edit_profile, null);
        }

        final CheckedTextView itemEditProfile = (CheckedTextView) convertView.findViewById(R.id.item_edit_profile);
        itemEditProfile.setText(mData[position]);
        itemEditProfile.setChecked(mData[position].equals(itemSelected));

        itemEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemEditProfile.setChecked(!itemEditProfile.isChecked());
                Intent data = new Intent();
                data.putExtra("relationship", position);
                mDialog.getTargetFragment().onActivityResult(0, Activity.RESULT_OK, data);
                mDialog.dismiss();
            }
        });

        return convertView;
    }
}
