package com.ntq.project.activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ntq.project.R;

public class LoginActivity extends ActionBarActivity implements View.OnClickListener{

    public static final String EMAIL = "tanpt";
    public static final String PASSWORD = "123";

    private EditText editEmail, editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnActionLeft = (Button) findViewById(R.id.btn_left);
        Button btnActionRight = (Button) findViewById(R.id.btn_right);
        TextView tvTitleAction = (TextView) findViewById(R.id.tv_title);

        tvTitleAction.setText(R.string.common_app_name);
        btnActionRight.setBackgroundResource(R.drawable.btn_action_bar);
        btnActionRight.setText(R.string.login_forgot);
        btnActionLeft.setBackgroundResource(R.drawable.btn_nav_back);

        btnActionLeft.setOnClickListener(this);
        btnActionRight.setOnClickListener(this);

        editEmail = (EditText) findViewById(R.id.edit_email);
        editPassword = (EditText) findViewById(R.id.edit_password);

        Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                onBackPressed();
                break;
            case R.id.btn_right:
                break;
            case R.id.btn_login:
                String email = editEmail.getText().toString().trim();
                String password = editPassword.getText().toString().trim();
                login(email, password);
                break;
        }
    }

    @TargetApi(11)
    private void login(String email, String password) {
        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Please enter email or password", Toast.LENGTH_LONG).show();
        }
        else {
            if (email.equals(EMAIL) && password.equals(PASSWORD)) {
                SharedPreferences prefs = getSharedPreferences(AndG.PREFS_FILE_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(AndG.PREF_IS_LOGIN, true);
                editor.putString(AndG.PREF_USER_NAME, email);
                editor.commit();

                Intent intent = new Intent(LoginActivity.this, AndG.class);
                intent.putExtra("email", email);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Error")
                        .setMessage("Email or password is wrong")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create().show();
            }
        }
    }
}
