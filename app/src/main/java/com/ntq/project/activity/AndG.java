package com.ntq.project.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;
import com.ntq.project.R;
import com.ntq.project.adapter.FriendsListChatAdapter;
import com.ntq.project.fragment.FragmentAuction;
import com.ntq.project.fragment.FragmentGiveGift;
import com.ntq.project.fragment.FragmentNotifications;
import com.ntq.project.fragment.FragmentPoints;
import com.ntq.project.fragment.FragmentSearchFriends;
import com.ntq.project.fragment.FragmentSetting;
import com.ntq.project.fragment.FragmentRecentReport;
import com.ntq.project.fragment.FragmentEditProfile;
import com.ntq.project.interfaces.OnClickActionBarListener;

import java.util.ArrayList;
import java.util.List;

public class AndG extends SlidingActivity implements View.OnClickListener {

    public static final int NAV_NONE = R.color.bg_action_bar;
    public static final int NAV_TEXT = R.drawable.btn_action_bar;
    public static final int NAV_MENU = R.drawable.btn_nav_menu;
    public static final int NAV_BACK = R.drawable.btn_nav_back;
    public static final int NAV_MESSAGE = R.drawable.btn_nav_message;

    public static final String PREFS_FILE_NAME = "andG";
    public static final String PREF_IS_LOGIN = "isLogin";
    public static final String PREF_USER_NAME = "userName";
    public static final String PREF_TOTAL_POINTS = "totalPoints";

    private SlidingMenu slidingMenu;

    private Button btnActionLeft, btnActionRight;
    private TextView tvTitleAction, tvNotifications;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_andg);

        setBehindContentView(R.layout.activity_main);

        slidingMenu = getSlidingMenu();

        slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        slidingMenu.setBehindOffsetRes(R.dimen.offset);

        slidingMenu.setSecondaryMenu(R.layout.activity_chat);

        btnActionLeft = (Button) findViewById(R.id.btn_left);
        btnActionRight = (Button) findViewById(R.id.btn_right);
        tvTitleAction = (TextView) findViewById(R.id.tv_title);
        tvNotifications = (TextView) findViewById(R.id.tv_notifications);

        btnActionLeft.setOnClickListener(this);
        btnActionRight.setOnClickListener(this);

        getViewsMenuLeft();
        getViewsMenuRight();

        Fragment fragment = new FragmentSetting();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_body, fragment, fragment.getClass().getName())
                .commit();

        SharedPreferences prefs = getSharedPreferences(PREFS_FILE_NAME, MODE_PRIVATE);

        if (!prefs.getBoolean(PREF_IS_LOGIN, false)) {
            Intent intent = new Intent(this, SplashScreen.class);
            startActivity(intent);
            finish();
        }
    }

    private void getViewsMenuLeft() {
        TextView textRecentReport = (TextView) findViewById(R.id.text_recent_report);
        TextView textSearchFriend = (TextView) findViewById(R.id.text_search_friends);
        TextView textProfile = (TextView) findViewById(R.id.text_profile);
        TextView textChat = (TextView) findViewById(R.id.text_chat);
        TextView textNotification = (TextView) findViewById(R.id.text_notifications);
        TextView textPoints = (TextView) findViewById(R.id.text_points);
        TextView textSetting = (TextView) findViewById(R.id.text_setting);
        TextView textGiveGift = (TextView) findViewById(R.id.text_give_gift);
        TextView textAuction = (TextView) findViewById(R.id.text_auciton);

        textRecentReport.setOnClickListener(this);
        textSearchFriend.setOnClickListener(this);
        textProfile.setOnClickListener(this);
        textChat.setOnClickListener(this);
        textNotification.setOnClickListener(this);
        textPoints.setOnClickListener(this);
        textSetting.setOnClickListener(this);
        textGiveGift.setOnClickListener(this);
        textAuction.setOnClickListener(this);
    }

    private TextView textRandomChat;
    private TextView textAddFriends;
    private TextView textDeleteHistory;

    public static boolean isDeleteHistory = false;

    private FriendsListChatAdapter adapter;

    private void getViewsMenuRight() {
        ListView lvChats = (ListView) findViewById(R.id.lv_chats);
        List<String> data = new ArrayList<>();
        for (int i = 1; i <= 15; i++) {
            data.add(i%2+"");
        }
        adapter = new FriendsListChatAdapter(this, data);
        lvChats.setAdapter(adapter);

        textRandomChat = (TextView) findViewById(R.id.text_random_chat);
        textAddFriends = (TextView) findViewById(R.id.text_add_friend);
        textDeleteHistory = (TextView) findViewById(R.id.text_delete_history);

        textRandomChat.setOnClickListener(this);
        textAddFriends.setOnClickListener(this);
        textDeleteHistory.setOnClickListener(this);
    }

    private void changeButtonMenuRight(boolean flag) {
        if (!flag) {
            textDeleteHistory.setText(R.string.common_done);
            textRandomChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_mark_readed, 0, 0);
            textRandomChat.setText(R.string.sliding_menu_right_top_txt_mark_readed);
            textAddFriends.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_delete_all, 0, 0);
            textAddFriends.setText(R.string.sliding_menu_right_top_txt_delete_all);
        }
        else {
            textDeleteHistory.setText(R.string.sliding_menu_right_top_txt_edit);
            textRandomChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_shaketochat, 0, 0);
            textRandomChat.setText(R.string.shake_to_chat_title);
            textAddFriends.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_add_friends, 0, 0);
            textAddFriends.setText(R.string.sliding_menu_right_top_txt_add_friend);
        }
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_body, fragment, fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName())
                .commit();

        slidingMenu.showContent();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public void setTitle(int titleId) {
        tvTitleAction.setText(getString(titleId));
    }

    public void setTitle(String title) {
        tvTitleAction.setText(title);
    }

    public void setButtonActionLeft(int image_id, String title) {
        btnActionLeft.setBackgroundResource(image_id);
        btnActionLeft.setText(title);
    }

    public void setButtonActionRight(int image_id, String title) {
        btnActionRight.setBackgroundResource(image_id);
        btnActionRight.setText(title);
        if (image_id != NAV_MESSAGE)
            tvNotifications.setVisibility(View.GONE);
    }

    public void showNotifications(int numOfNotif) {
        if (numOfNotif != 0) {
            tvNotifications.setVisibility(View.VISIBLE);
            tvNotifications.setText(numOfNotif + "");
        }
    }

//    public void hideNotification() {
//        tvNotifications.setVisibility(View.GONE);
//    }

    public OnClickActionBarListener listener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                listener.onClickActionBarLeft();
                break;
            case R.id.btn_right:
                listener.onClickActionBarRight();
                break;
            case R.id.text_recent_report:
                replaceFragment(new FragmentRecentReport());
                break;
            case R.id.text_search_friends:
                replaceFragment(new FragmentSearchFriends());
                break;
            case R.id.text_auciton:
                replaceFragment(new FragmentAuction());
                break;
            case R.id.text_profile:
                replaceFragment(new FragmentEditProfile());
                break;
            case R.id.text_chat:
                slidingMenu.showSecondaryMenu();
                break;
            case R.id.text_notifications:
                replaceFragment(new FragmentNotifications());
                break;
            case R.id.text_points:
                replaceFragment(new FragmentPoints());
                break;
            case R.id.text_setting:
                replaceFragment(new FragmentSetting());
                break;
            case R.id.text_give_gift:
                replaceFragment(new FragmentGiveGift());
                break;
            case R.id.text_delete_history:
                changeButtonMenuRight(isDeleteHistory);
                isDeleteHistory = !isDeleteHistory;
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.toggle();
        } else {
            super.onBackPressed();
        }
    }

}
