package com.ntq.project.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.adapter.EthnicityListAdapter;
import com.ntq.project.interfaces.OnClickActionBarListener;

import java.util.HashMap;

public class FragmentSearchFriendEditEthnicity extends Fragment {

    private SharedPreferences prefs;

    private String[] listEthnicity;

    private HashMap<String, Boolean> listEthnicityCheck;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search_ethnicity, container, false);

        AndG parent = (AndG) getActivity();
        parent.setTitle(R.string.ethnicity_title);
        parent.setButtonActionLeft(AndG.NAV_TEXT, getString(R.string.common_cancel));
        parent.setButtonActionRight(AndG.NAV_TEXT, getString(R.string.common_done));

        prefs = getActivity().getSharedPreferences(FragmentSearchFriendSetting.PREFS_FILE_NAME, Context.MODE_PRIVATE);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                getFragmentManager().popBackStack();
            }

            @Override
            public void onClickActionBarRight() {
                done();
            }
        };

        ListView lv_ethnictiy = (ListView) view.findViewById(R.id.lv_ethnicity);

        listEthnicity = getResources().getStringArray(R.array.ethnicity_list);
        getListEthnicityCheck();

        EthnicityListAdapter adapter = new EthnicityListAdapter(getActivity(), listEthnicityCheck);
        lv_ethnictiy.setAdapter(adapter);

        return view;
    }

    private void getListEthnicityCheck() {
        listEthnicityCheck = new HashMap<>();
        for (String s : listEthnicity) {
            listEthnicityCheck.put(s, prefs.getBoolean(s, false));
        }
    }

    private void done() {
        SharedPreferences.Editor editor = prefs.edit();
        for (String s : listEthnicity) {
            editor.putBoolean(s, listEthnicityCheck.get(s));
        }
        editor.commit();
        Toast.makeText(getActivity(), "Saved success", Toast.LENGTH_LONG).show();
        getFragmentManager().popBackStack();
    }

}
