package com.ntq.project.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ntq.project.R;
import com.ntq.project.activity.AndG;
import com.ntq.project.adapter.NotificationsListAdapter;
import com.ntq.project.interfaces.OnClickActionBarListener;

public class FragmentNotifications extends Fragment {

    private AndG parent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_notifications, container, false);

        parent = (AndG) getActivity();
        parent.setTitle(R.string.notification_title);
        parent.setButtonActionLeft(AndG.NAV_MENU, "");
        parent.setButtonActionRight(AndG.NAV_MESSAGE, "");
        parent.showNotifications(4);

        parent.listener = new OnClickActionBarListener() {
            @Override
            public void onClickActionBarLeft() {
                parent.getSlidingMenu().showMenu();
            }

            @Override
            public void onClickActionBarRight() {
                parent.getSlidingMenu().showSecondaryMenu();
            }
        };

        String[] data = new String[] {"1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4"};

        ListView lvNotifications = (ListView) view.findViewById(R.id.lv_notifications);
        NotificationsListAdapter adapter = new NotificationsListAdapter(getActivity(), data);
        lvNotifications.setAdapter(adapter);

        return view;
    }

}
